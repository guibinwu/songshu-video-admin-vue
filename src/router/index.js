import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/admin/dashboard'
    },
    {
      path: '/admin',
      name: 'HelloWorld',
      component: () => import('../components/common/Home.vue'),
      children: [

        {
          path: 'dashboard',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Dashboard.vue'),
          meta: {title: '系统首页'}
        }, {
          path: 'user-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/List.vue'),
          meta: {title: '用户列表'}
        },
        {
          path: 'invite-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/Invite.vue'),
          meta: {title: '推广记录'}
        },
        {
          path: 'change-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/Change.vue'),
          meta: {title: '账变记录'}
        },
        {
          path: 'withdraw-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/Withdraw.vue'),
          meta: {title: '账变记录'}
        },
        {
          path: 'user-log',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/Log.vue'),
          meta: {title: '用户日志'}
        },{
          path: 'user-comment',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/Comment.vue'),
          meta: {title: '评论列表'}
        },  {
          path: 'user-action-log',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/User/LogAction.vue'),
          meta: {title: '用户操作日志'}
        }, {
          path: 'video-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Video/List.vue'),
          meta: {title: '视频列表'}
        }, {
          path: 'text-image-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/TextImage/List.vue'),
          meta: {title: '图文列表'}
        }, {
          path: 'cipher-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/cipher/List.vue'),
          meta: {title: '卡密列表'}
        }, {
          path: 'config-basic',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Basic.vue'),
          meta: {title: '基本配置'}
        }, {
          path: 'config-sms',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Sms.vue'),
          meta: {title: '短信配置'}
        }, {
          path: 'config-mail',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Mail.vue'),
          meta: {title: '邮件配置'}
        },{
          path: 'config-ext',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Extra.vue'),
          meta: {title: '附件配置'}
        },{
          path: 'config-video',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Video.vue'),
          meta: {title: '视频配置'}
        },{
          path: 'config-payment',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Payment.vue'),
          meta: {title: '支付配置'}
        }, {
          path: 'config-seo',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Seo.vue'),
          meta: {title: 'SEO配置'}
        }, {
          path: 'login-wechat',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Wechat.vue'),
          meta: {title: '微信登录设置'}
        }, {
          path: 'login-qq',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/QQ.vue'),
          meta: {title: 'QQ登录设置'}
        }, {
          path: 'login-weibo',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Weibo.vue'),
          meta: {title: '微博登录设置'}
        }, {
          path: 'upload-qiniu',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/QiNiu.vue'),
          meta: {title: '七牛云配置'}
        }, {
          path: 'upload-aliyun',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Aliyun.vue'),
          meta: {title: '阿里云配置'}
        }, {
          path: 'upload-ftp',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/FTP.vue'),
          meta: {title: 'FTP配置'}
        },{
          path: 'safe-cy',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/SafeCy.vue'),
          meta: {title: '护驾卫士'}
        }, {
          path: 'config-comment',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Config/Comment.vue'),
          meta: {title: '评论配置'}
        }, {
          path: 'admin-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Admin/List.vue'),
          meta: {title: '管理员列表'}
        }, {
          path: 'admin-log',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Admin/Log.vue'),
          meta: {title: '管理员日志'}
        }, {
          path: 'type-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Type/List.vue'),
          meta: {title: '分类管理'}
        }, {
          path: 'advert-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Advert/List.vue'),
          meta: {title: '广告列表'}
        }, {
          path: 'update-manager',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Update/List.vue'),
          meta: {title: 'APP版本管理'}
        }, {
          path: 'vip-shop-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Vip/VipShop.vue'),
          meta: {title: 'VIP商品管理'}
        }, {
          path: 'vip-payment-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Vip/PayType.vue'),
          meta: {title: '支付方式管理'}
        }, {
          path: 'order-list',
          component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Order/List.vue'),
          meta: {title: '订单管理'}
        },{
          path:'*',
          component:()=>import('../components/page/404.vue')
        }
      ]
    }
    ,
    {
      path:'*',
      component:()=>import('../components/page/404.vue')
    },
    {
      name:"login",
      path: '/login',
      component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
      meta: {title: '登录'}
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})
