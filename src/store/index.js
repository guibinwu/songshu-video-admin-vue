import Vue from 'vue'
import Vuex from 'vuex'
import {baseURL,frontURL} from '../api/config'
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    user:{},
    baseUrl:baseURL,
    frontUrl:frontURL
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
  },
  actions: {
    isLogin(context,vue){
      var user = JSON.parse(localStorage.getItem('admin'))

      if(user){
        context.commit('setUser',user)
      }
      return new Promise(((resolve, reject) => {
        var res =vue.$api.login.isLogin().then((res)=>{
          resolve(res);
        })
      }))
    }
  },
  modules: {}
})
export default store
