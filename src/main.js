// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import '../node_modules/element-ui/lib/theme-chalk/index.css'
import './assets/css/icon.css'
import './components/common/directives'
import utils from './utils'
import api from './api'
import JSencrypt from './utils/encrypt'
Vue.prototype.$decrypt = JSencrypt;
//销售客服QQ：422108995 伊凡
Vue.prototype.$xskf = 'j9AHRJfa5lqItRS83vUhWG1CVbwJax2UWqo0zHFcvc12EtTAq3N/5eVXCYKP8/3eYk82KpNEIPNZf8VreNMa8g2De48dsZs7' +
  'WY7xLeLUdOj+lenW9qduw76tJonrc4D3YLMcLR37oGRCA+8xNTH9MS6UA4aaB9NiqF5RVRpqfUo=';
//https://songshu.youyacao.com/video.html
Vue.prototype.$video_url = 'zBh3lD2jbvIQRzgOGjf9Z3TTQ7bXOor17NoZNo1Hu0gz/SQHNx0u4IShAk/i3m7RYIC7sqyZSHUFCfzXB4FCE8mzimL' +
  'M7YK6SXH4Ig7J50Vtk278uB587a2Hd+G8Ko1VDl0Tbel4cdtX6o1rCCVmNzoaRnYdx6WiWmutHcYI7lI=';
//松鼠短视频系统
Vue.prototype.$video_title = 'yj45KMYPEsZEAUe8mdFHfhIdqe0ZFsc5Ma/51PUPU36nJ9Eb5pJ5J3bzGJdXwIHov0Frq0AJIV+sZsHNewhn0Ucm7' +
  'wuToPOckpF+NaaV5LHkYdJwrBekanvkk4FyryyfqqUfpd8Y4sblQd9Xcb4i72G9Lc28n8xrMlwtLiLTWSc=';
//https://www.youyacao.com/
Vue.prototype.$yyc_url = 'pKayqmD74HSKnqSeuCq6ZfrRFsa7Z0IbcjCUz4w60V/+iYShxTOFlo+xokvOe9Hxyj9KoQdlY5tVZFacRrckvhCBkkOJ9' +
  'tDEjoOQoFtC8quIQrYyWSltEfZrccBSvAnGjReTntU/ZWC8XGM752WYHTXnHL8118DSrOKdAAXSSpM=';
//成都市一颗优雅草科技有限公司
Vue.prototype.$copy_right = 'gdDaS8lU/RKDsG3CX9QEAw2uqV/XuPL1cnRUb+bCR51gfIst77TC5Kcio2l8dnw10AgGor3ty6wjIxCIxpiL/qyVqI' +
  'CmATd2w1j7FCs/PSC4NhVlk4JTbxhZrfQRx9EWbBqzj5dPKB4IWbYHLzu4ThkfIq3FOaQ3v+NCCrDUHv0=';
//联系公司座机：028-61072556
Vue.prototype.$gszj = 'Z+ub8KnSKnnITrPgbL9NhcGUmncpdq4y5psegq+q7ZdsTiFwbGgXr7Xl27UM8E78EE+iT5cnmA30XCCHgSbhyFdrkQ8X1DbY' +
  'ZAMRGVI4f5GTChOEQOUUDl7bkDhMHeCEyj05g623ehauaQpGiW1RCulwQa3A0AuFJCE645BStws=';
//加密解密模块
/*
var test = JSencrypt.encrypt("https://songshu.youyacao.com/video.html");
console.log("encode:"+test);
var test_de = JSencrypt.decrypt('5EsFxA2DCxrKzQXBAJThOXFgo4AI4zV2GZQTNIhGfh9OyTPuxD44qrigu2FjIS8zQd65cfZ3CTtJ4qvl88DvLkqLuqN0i6qfUnsz/m15UzWxQCXRwz7kmGKUDpYtfP4SOCaS7AE/+rMP8qC88rV/xIs2tcuzxnR3DWSRWDWht2U=');
console.log("decode:"+test_de);
*/



  Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$api = api
import store from './store'
Vue.prototype.$utils = utils
Vue.prototype.$store = store
// 使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | YYC松鼠短视频系统后台管理-一颗优雅草科技`
  const role = localStorage.getItem('admin')
  if (!role && to.path !== '/login') {
    next('/login')
  } else if (to.meta.permission) {
    // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
    role.username === 'admin' ? next() : next('/403')
  } else {
    // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
    if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
      Vue.prototype.$alert('不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
        confirmButtonText: '确定'
      })
    } else {
      next()
    }
  }
})
/* eslint-disable no-new */

window.vue = new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>',
  render: h => h(App)
})
